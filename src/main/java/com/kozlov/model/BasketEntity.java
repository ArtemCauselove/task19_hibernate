package com.kozlov.model;

import javax.persistence.*;

@Entity
@Table(name = "basket", schema = "store_jdbc", catalog = "")
public class BasketEntity {
    private Integer id;
    private Integer ammount;
    private Integer cost;

    @Id
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ammount")
    public Integer getAmmount() {
        return ammount;
    }

    public void setAmmount(Integer ammount) {
        this.ammount = ammount;
    }

    @Basic
    @Column(name = "cost")
    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BasketEntity that = (BasketEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (ammount != null ? !ammount.equals(that.ammount) : that.ammount != null) return false;
        if (cost != null ? !cost.equals(that.cost) : that.cost != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (ammount != null ? ammount.hashCode() : 0);
        result = 31 * result + (cost != null ? cost.hashCode() : 0);
        return result;
    }
}
