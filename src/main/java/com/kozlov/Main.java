package com.kozlov;

import com.kozlov.model.ClientsEntity;
import org.hibernate.HibernateException;
import org.hibernate.Metamodel;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.metamodel.EntityType;

import java.util.Map;

public class Main {
    private static final SessionFactory ourSessionFactory;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();
            ourSessionFactory = configuration.buildSessionFactory();

        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }

    public static void main(final String[] args) throws Exception {
        final Session session = getSession();
        Query query;
        try {
            session.beginTransaction();
            ClientsEntity cityEntity = new ClientsEntity(3,"artem","kozlov","sd@gmail.com",2,"1",1);;
            session.save(cityEntity);
            session.getTransaction().commit();
            query = session.createQuery("from " + "ClientsEntity");
            for (Object obj : query.list()) {
                ClientsEntity client = (ClientsEntity) obj;
                System.out.format("%3d %-18s %-18s %s\n", client.getId(),
                        client.getName(), client.getEmail(), client.getMoney());}

        } finally {
            session.close();
        }
    }
}