package com.kozlov.model;

import javax.persistence.*;

@Entity
@Table(name = "ship", schema = "store_jdbc", catalog = "")
public class ShipEntity {
    private Integer id;
    private Integer shipCost;
    private String fromAdress;
    private String toAdress;
    private String postService;

    @Id
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ship_cost")
    public Integer getShipCost() {
        return shipCost;
    }

    public void setShipCost(Integer shipCost) {
        this.shipCost = shipCost;
    }

    @Basic
    @Column(name = "from_adress")
    public String getFromAdress() {
        return fromAdress;
    }

    public void setFromAdress(String fromAdress) {
        this.fromAdress = fromAdress;
    }

    @Basic
    @Column(name = "to_adress")
    public String getToAdress() {
        return toAdress;
    }

    public void setToAdress(String toAdress) {
        this.toAdress = toAdress;
    }

    @Basic
    @Column(name = "post_service")
    public String getPostService() {
        return postService;
    }

    public void setPostService(String postService) {
        this.postService = postService;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ShipEntity that = (ShipEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (shipCost != null ? !shipCost.equals(that.shipCost) : that.shipCost != null) return false;
        if (fromAdress != null ? !fromAdress.equals(that.fromAdress) : that.fromAdress != null) return false;
        if (toAdress != null ? !toAdress.equals(that.toAdress) : that.toAdress != null) return false;
        if (postService != null ? !postService.equals(that.postService) : that.postService != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (shipCost != null ? shipCost.hashCode() : 0);
        result = 31 * result + (fromAdress != null ? fromAdress.hashCode() : 0);
        result = 31 * result + (toAdress != null ? toAdress.hashCode() : 0);
        result = 31 * result + (postService != null ? postService.hashCode() : 0);
        return result;
    }
}
