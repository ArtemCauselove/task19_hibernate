package com.kozlov.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "discount_cards", schema = "store_jdbc", catalog = "")
public class DiscountCardsEntity {
    private Integer id;
    private String name;
    private Integer percent;
    private Integer client_id;
    public DiscountCardsEntity(Integer id, String name, Integer percent, Integer client_id){
        this.id = id;
        this.name = name;
        this.percent = percent;
        this.client_id = client_id;
    }
    public DiscountCardsEntity(){}
    @Id
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "percent")
    public Integer getPercent() {
        return percent;
    }

    public void setPercent(Integer percent) {
        this.percent = percent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DiscountCardsEntity that = (DiscountCardsEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (percent != null ? !percent.equals(that.percent) : that.percent != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (percent != null ? percent.hashCode() : 0);
        return result;
    }


    public Integer getClient_id() {
        return client_id;
    }

    public void setClient_id(Integer client_id) {
        this.client_id = client_id;
    }
}
